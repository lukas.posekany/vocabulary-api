package routes

import (
	"encoding/json"
	"net/http"

	"gitlab.com/lukas.posekany/vocabulary-api/src/controllers"
	"gitlab.com/lukas.posekany/vocabulary-api/src/models"
)

// GetWords will get array of ids for words user want, get words and return
func GetWords(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	IDsString := r.Form.Get("wordsIDs")
	var wordsIDs []string
	err := json.Unmarshal([]byte(IDsString), &wordsIDs)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	if len(wordsIDs) == 0 {
		w.Write([]byte("No IDs provided"))
		return
	}

	words, err := controllers.GetWords(wordsIDs)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	wordsJson, err := json.Marshal(words)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write([]byte(wordsJson))
}

func DeleteWords(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	wordsID := r.Form.Get("wordsID")

	err := controllers.DeleteWords(wordsID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write([]byte("OK"))
}

// EditWords will get words from request body, parse it to models.Words interface
// and try to save it to databse
// WORKS ALSO FOR ADDING WORDS
func ProcessWords(w http.ResponseWriter, r *http.Request) {

	var words models.Words
	err := json.NewDecoder(r.Body).Decode(&words)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	ID, err := controllers.ProcessWords(words)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Write([]byte(ID))
}
