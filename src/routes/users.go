package routes

import (
	"encoding/json"
	"net/http"

	"gitlab.com/lukas.posekany/vocabulary-api/src/controllers"
	"gitlab.com/lukas.posekany/vocabulary-api/src/models"
)

func GetAllUsers(w http.ResponseWriter, r *http.Request) {
	users, err := controllers.GetAllUsers()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	for i := range users {
		users[i].Password = ""
	}
	usersJson, err := json.Marshal(users)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(usersJson)
}

func ProcessUser(w http.ResponseWriter, r *http.Request) {
	var user models.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	ID, err := controllers.ProcessUser(user)
	if err != nil {
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte(ID))
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	userID := r.Form.Get("userID")

	err := controllers.DeleteUser(userID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write([]byte("OK"))
}

func UpdateUserArray(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	userID := r.Form.Get("userID")
	set := r.Form.Get("set")
	element := r.Form.Get("element")
	deleteStr := r.Form.Get("delete")
	var delete bool
	if deleteStr == "true" {
		delete = true
	} else if deleteStr == "false" {
		delete = false
	} else {
		http.Error(w, "not valide delete parameter", http.StatusInternalServerError)
		return
	}

	err := controllers.UpdateUserArray(userID, set, element, delete)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write([]byte("OK"))
}
