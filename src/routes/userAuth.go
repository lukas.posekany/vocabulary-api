package routes

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/gorilla/sessions"
	"gitlab.com/lukas.posekany/vocabulary-api/src/controllers"
	"gitlab.com/lukas.posekany/vocabulary-api/src/logs"
)

var store = sessions.NewCookieStore([]byte("LWx5}V]Z"))

// GetCurrentUser returns informations about current user if he is logged in
func GetCurrentUser(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r, "session")
	if err != nil {
		logs.Error.Println("[GetCurrentUser1]: CANNOT GET STORAGE", err)
		return
	}
	id, ok := session.Values["id"].(string)
	if !ok {
		http.Error(w, "Forbidden", http.StatusForbidden)
		return
	}

	user, err := controllers.GetUserById(id)
	if err != nil {
		http.Error(w, "Forbidden", http.StatusForbidden)
		return
	}

	user.Password = ""
	user.ID = id
	userJson, err := json.Marshal(user)
	if err != nil {
		logs.Error.Println("[GetCurrentUser2]: ", err)
		http.Error(w, "unable to parse user", http.StatusInternalServerError)
	}

	w.Write(userJson)
}

func Login(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	session, err := store.Get(r, "session")
	if err != nil {
		// DO NOTHING, NEW SESSION WILL BE SAVED ANYWAY
		// log.Println("CANNOT GET SESSION", err)
		// http.Error(w, "Internal Error", http.StatusInternalServerError)
		// return
	}

	nameEmail := strings.TrimSpace(r.Form.Get("nameEmail"))
	password := strings.TrimSpace(r.Form.Get("password"))

	user, err := controllers.CheckUser(nameEmail, password)
	if err != nil {
		http.Error(w, "wrong user or password", http.StatusUnauthorized)
		return
	}

	// Set user as authenticated
	session.Values["authenticated"] = true
	session.Values["role"] = user.Role
	session.Values["name"] = user.Name
	session.Values["email"] = user.Email
	session.Values["id"] = user.ID
	err = session.Save(r, w)
	if err != nil {
		fmt.Println("Login Error", err)
	}

	userJson, err := json.Marshal(user)
	if err != nil {
		http.Error(w, "cannot parse user", http.StatusInternalServerError)
	}
	w.Write(userJson)
}

func Logout(w http.ResponseWriter, r *http.Request) {

	session, err := store.Get(r, "session")
	if err != nil {
		http.Error(w, "Internal Error", http.StatusInternalServerError)
		return
	}

	// Revoke users authentication
	session.Values["authenticated"] = false
	session.Values["role"] = ""
	session.Values["name"] = ""
	session.Values["email"] = ""
	session.Values["id"] = ""
	session.Save(r, w)
	w.Write(([]byte("OK")))
}

func UpdatePwd(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	nameEmail := strings.TrimSpace(r.Form.Get("nameEmail"))
	oldPassword := strings.TrimSpace(r.Form.Get("oldPassword"))
	newPassword := strings.TrimSpace(r.Form.Get("newPassword"))
	if !controllers.UpdatePassword(nameEmail, oldPassword, newPassword) {
		w.Write([]byte("not changed"))
		return
	}
	w.Write([]byte("ok"))
}
