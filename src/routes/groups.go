package routes

import (
	"encoding/json"
	"net/http"

	"gitlab.com/lukas.posekany/vocabulary-api/src/controllers"
	"gitlab.com/lukas.posekany/vocabulary-api/src/models"
)

func GetGroups(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	IDsString := r.Form.Get("groupsIDs")
	var groupsIDs []string
	err := json.Unmarshal([]byte(IDsString), &groupsIDs)
	if err != nil {
		w.Write([]byte(err.Error()))
	}

	if len(groupsIDs) == 0 {
		w.Write([]byte("No IDs provided"))
		return
	}

	groups, err := controllers.GetGroups(groupsIDs)
	if err != nil {
		w.Write([]byte(err.Error()))
		return
	}

	groupsJson, err := json.Marshal(groups)
	if err != nil {
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte(groupsJson))
}

func DeleteGroup(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	groupID := r.Form.Get("groupID")

	err := controllers.DeleteGroup(groupID)
	if err != nil {
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("OK"))
}

func ProcessGroup(w http.ResponseWriter, r *http.Request) {
	var group models.Group
	err := json.NewDecoder(r.Body).Decode(&group)
	if err != nil {
		w.Write([]byte(err.Error()))
		return
	}

	ID, err := controllers.ProcessGroup(group)
	if err != nil {
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte(ID))
}

func AddToGroup(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	wordsID := r.Form.Get("wordsID")
	groupID := r.Form.Get("groupID")
	addStr := r.Form.Get("add")

	if wordsID == "" || groupID == "" || addStr == "" {
		w.Write([]byte("NOT POSSIBLE"))
		return
	}

	var add bool
	if addStr == "true" {
		add = true
	} else if addStr == "false" {
		add = false
	} else {
		w.Write([]byte("not valide add parameter"))
		return
	}

	if err := controllers.AddToGroups(groupID, wordsID, add); err != nil {
		w.Write([]byte(err.Error()))
		return
	}

	w.Write([]byte("OK"))
}
