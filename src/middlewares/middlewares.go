package middlewares

import (
	"net/http"
	"os"

	"github.com/gorilla/sessions"
	"gitlab.com/lukas.posekany/vocabulary-api/src/logs"
)

var (
	domain string
	store  = sessions.NewCookieStore([]byte("LWx5}V]Z"))
)

func init() {
	domain = os.Getenv("ORIGIN")
	if len(domain) == 0 {
		domain = "http://localhost:3000"
		// adminDomain = "http://localhost:8080"
	}
}

func CorsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "OPTIONS" { // preflight
			w.Write([]byte("OK"))
			return
		}
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "*")

		next.ServeHTTP(w, r)
	})
}

func AuthenticatedMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", domain)
		w.Header().Set("Access-Control-Allow-Headers", "withcredentials, access-control-allow-credentials, Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers")
		w.Header().Set("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		if r.Method == "OPTIONS" { // preflight
			// w.Write([]byte("OK"))
			return
		}

		session, err := store.Get(r, "session")
		if err != nil {
			logs.Error.Println("Cannot get store: ", err)
			return
		}

		// check if user is authenticated
		if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}
		_, ok := session.Values["id"].(string)
		if !ok {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}
		next.ServeHTTP(w, r)
	})
}

func SuperAuthenticatedMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", domain)
		w.Header().Set("Access-Control-Allow-Headers", "withcredentials, access-control-allow-credentials, Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers")
		w.Header().Set("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		if r.Method == "OPTIONS" { // preflight
			w.Write([]byte("OK"))
			return
		}
		session, err := store.Get(r, "session")
		if err != nil {
			logs.Error.Println("CANNOT GET STORAGE", err)
			return
		}

		// check if user is authenticated
		if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}
		role, ok := session.Values["role"].(string)
		if !ok {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}
		if role != "admin" {
			http.Error(w, "Forbidden", http.StatusForbidden)
			return
		}

		next.ServeHTTP(w, r)
	})
}

func UserCorsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", domain)
		w.Header().Set("Access-Control-Allow-Headers", "withcredentials, access-control-allow-credentials, Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers")
		w.Header().Set("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		if r.Method == "OPTIONS" { // preflight
			w.Write([]byte("OK"))
			return
		}
		next.ServeHTTP(w, r)
	})
}
