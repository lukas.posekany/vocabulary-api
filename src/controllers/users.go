package controllers

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"gitlab.com/lukas.posekany/vocabulary-api/src/dtb"
	"gitlab.com/lukas.posekany/vocabulary-api/src/logs"
	"gitlab.com/lukas.posekany/vocabulary-api/src/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/crypto/bcrypt"
)

func GetAllUsers() (users []models.User, err error) {
	usersFilter := bson.D{{}}

	cur, err := dtb.UsersColl.Find(context.TODO(), usersFilter, options.Find())
	if err != nil {
		return users, err
	}
	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()) {
		var user models.User
		err := cur.Decode(&user)
		if err != nil {
			return users, err
		}
		user.ID = user.ObjectID.Hex()
		users = append(users, user)
	}
	return users, err
}

// ProcessUser decided if user is already in database
// if user is in UPDATE, if not ADD
func ProcessUser(U models.User) (ID string, err error) {
	if U.ID == "" {
		ID, err := AddUser(U)
		return ID, err
	}
	return updateUser(U)
}

func DeleteUser(userID string) error {
	oid, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		return err
	}

	res, err := dtb.UsersColl.DeleteOne(context.TODO(), bson.M{"_id": oid})

	if err != nil {
		return err
	} else {
		if res.DeletedCount == 0 {
			return errors.New("id not found")
		}
	}
	return nil
}

// UpdateUserArray serves to update words, groups, favorite, which are arrays containing ids of words
func UpdateUserArray(userID string, set string, element string, delete bool) error {
	oid, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		return err
	}
	if set != "words" && set != "favorite" && set != "favoriteGroups" && set != "groups" {
		return errors.New("not known set")
	}
	filter := bson.M{"_id": bson.M{"$eq": oid}}
	var update primitive.D
	if delete {
		update = bson.D{
			{Key: "$pull", Value: bson.D{{Key: set, Value: element}}},
		}
	} else {
		update = bson.D{
			{Key: "$addToSet", Value: bson.D{{Key: set, Value: element}}},
		}
	}
	_, err = dtb.UsersColl.UpdateOne(context.TODO(), filter, update)
	return err
}

// updateWords will update words and return ID and err
func updateUser(U models.User) (ID string, err error) {
	objID, err := primitive.ObjectIDFromHex(U.ID)
	if err != nil {
		fmt.Println("UNABLE TO UPDATE USER", err)
		return "", err
	}

	var update primitive.M = bson.M{
		"$set": U.UserToBson(),
	}

	filter := bson.M{"_id": bson.M{"$eq": objID}}

	_, err = dtb.UsersColl.UpdateOne(
		context.Background(),
		filter,
		update,
	)

	if err != nil {
		return "", err
	}

	return ID, nil
}

// addWords will take words set, add it to database and return ID of new document with error
func AddUser(U models.User) (ID string, err error) {
	U.ObjectID = primitive.NewObjectID()

	// Hashing the password with the default cost of 10
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(U.Password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	U.Password = string(hashedPassword)

	result, err := dtb.UsersColl.InsertOne(context.TODO(), U)
	if err != nil {
		fmt.Println("addUser: ", err)
	}

	if oid, ok := result.InsertedID.(primitive.ObjectID); ok {
		id := strings.Split(oid.String(), "\"")[1]
		return id, nil
	} else {
		logs.Error.Println("[AddUser]: SOMETHING WRONG HAPPEND")
		return "", nil
	}
}
