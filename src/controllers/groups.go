package controllers

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"gitlab.com/lukas.posekany/vocabulary-api/src/dtb"
	"gitlab.com/lukas.posekany/vocabulary-api/src/logs"
	"gitlab.com/lukas.posekany/vocabulary-api/src/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func GetGroups(groupsIDs []string) (groups []models.Group, err error) {
	oids := make([]primitive.ObjectID, len(groupsIDs))
	for i := range groupsIDs {
		oids[i], err = primitive.ObjectIDFromHex(groupsIDs[i])
		if err != nil {
			logs.Error.Println("GetGroups1: ", err)
		}
	}
	filter := bson.M{"_id": bson.M{"$in": oids}}
	cur, err := dtb.GroupsColl.Find(context.TODO(), filter)
	if err != nil {
		return nil, err
	}
	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()) {
		var group models.Group
		err := cur.Decode(&group)
		if err != nil {
			logs.Error.Println("GetGroups2: ", err)
		}
		group.ID = group.ObjectID.Hex()
		groups = append(groups, group)
	}
	return groups, nil
}

func DeleteGroup(groupID string) error {
	oid, err := primitive.ObjectIDFromHex(groupID)
	if err != nil {
		return err
	}

	res, err := dtb.GroupsColl.DeleteOne(context.TODO(), bson.M{"_id": oid})

	if err != nil {
		return err
	} else {
		if res.DeletedCount == 0 {
			return errors.New("id not found")
		}
	}
	return nil
}

func ProcessGroup(G models.Group) (ID string, err error) {
	if G.ID == "" {
		ID, err := addGroup(G)
		return ID, err
	}
	return updateGroup(G)
}

func AddToGroups(groupID string, wordsID string, add bool) error {
	id, err := primitive.ObjectIDFromHex(groupID)
	if err != nil {
		return err
	}
	var update primitive.D
	if add {
		update = bson.D{
			{Key: "$addToSet", Value: bson.D{{Key: "vocabulary", Value: wordsID}}},
		}
	} else {
		update = bson.D{
			{Key: "$pull", Value: bson.D{{Key: "vocabulary", Value: wordsID}}},
		}
	}

	_, err = dtb.GroupsColl.UpdateOne(
		context.TODO(),
		bson.M{"_id": id},
		update,
	)
	return err
}

// updateWords will update words and return ID and err
func updateGroup(G models.Group) (ID string, err error) {
	objID, err := primitive.ObjectIDFromHex(G.ID)
	if err != nil {
		fmt.Println("updateGroup: ", err)
		return "", err
	}

	var update primitive.M = bson.M{
		"$set": G.GroupToBson(),
	}

	filter := bson.M{"_id": bson.M{"$eq": objID}}

	_, err = dtb.GroupsColl.UpdateOne(
		context.Background(),
		filter,
		update,
	)

	if err != nil {
		return "", err
	}

	return ID, nil
}

// addWords will take words set, add it to database and return ID of new document with error
func addGroup(G models.Group) (ID string, err error) {
	G.ObjectID = primitive.NewObjectID()
	result, err := dtb.GroupsColl.InsertOne(context.TODO(), G)
	if err != nil {
		logs.Error.Println("addGroup: ", err)
	}

	if oid, ok := result.InsertedID.(primitive.ObjectID); ok {
		id := strings.Split(oid.String(), "\"")[1]
		return id, nil
	} else {
		logs.Error.Println("SOMETHING WRONG HAPPEND")
		return "", nil
	}
}
