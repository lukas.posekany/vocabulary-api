package controllers

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"gitlab.com/lukas.posekany/vocabulary-api/src/dtb"
	"gitlab.com/lukas.posekany/vocabulary-api/src/logs"
	"gitlab.com/lukas.posekany/vocabulary-api/src/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// GetWords will reach to DTB and return words documents
func GetWords(wordsIDs []string) (words []models.Words, err error) {
	oids := make([]primitive.ObjectID, len(wordsIDs))
	for i := range wordsIDs {
		oids[i], err = primitive.ObjectIDFromHex(wordsIDs[i])
		if err != nil {
			logs.Error.Println("[GeWords1]: ", err)
		}
	}
	filter := bson.M{"_id": bson.M{"$in": oids}}
	cur, err := dtb.WordsColl.Find(context.TODO(), filter)
	if err != nil {
		return nil, err
	}
	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()) {
		var word models.Words
		err := cur.Decode(&word)
		if err != nil {
			logs.Error.Println("[GetWords2]", err)
		}
		word.ID = word.ObjectID.Hex()
		words = append(words, word)
	}

	return words, nil
}

func DeleteWords(wordsID string) error {
	oid, err := primitive.ObjectIDFromHex(wordsID)
	if err != nil {
		return err
	}

	res, err := dtb.WordsColl.DeleteOne(context.TODO(), bson.M{"_id": oid})

	if err != nil {
		return err
	} else {
		if res.DeletedCount == 0 {
			return errors.New("id not found")
		}
	}
	return nil
}

// ProcessWords will take words set, if it contains ID, it will edit words, if not, it will add words
func ProcessWords(W models.Words) (ID string, err error) {
	if W.ID == "" {
		ID, err := addWords(W)
		return ID, err
	}
	return updateWords(W)
}

// updateWords will update words and return ID and err
func updateWords(W models.Words) (ID string, err error) {
	objID, err := primitive.ObjectIDFromHex(W.ID)
	if err != nil {
		fmt.Println("updateWords: ", err)
		return "", err
	}

	var update primitive.M

	update = bson.M{
		"$set": W.WordsToBson(),
	}

	filter := bson.M{"_id": bson.M{"$eq": objID}}

	_, err = dtb.WordsColl.UpdateOne(
		context.Background(),
		filter,
		update,
	)

	if err != nil {
		return "", err
	}

	return W.ID, nil
}

// addWords will take words set, add it to database and return ID of new document with error
func addWords(W models.Words) (ID string, err error) {
	W.ObjectID = primitive.NewObjectID()
	result, err := dtb.WordsColl.InsertOne(context.TODO(), W)
	if err != nil {
		fmt.Println("addWords: ", err)
	}

	if oid, ok := result.InsertedID.(primitive.ObjectID); ok {
		id := strings.Split(oid.String(), "\"")[1]
		return id, nil
	} else {
		logs.Error.Println("[addWords]: SOMETHING WRONG HAPPEND")
		return "", nil
	}
}
