package controllers

import (
	"context"
	"errors"

	"gitlab.com/lukas.posekany/vocabulary-api/src/dtb"
	"gitlab.com/lukas.posekany/vocabulary-api/src/logs"
	"gitlab.com/lukas.posekany/vocabulary-api/src/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
)

func GetUserById(id string) (user models.User, err error) {
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return user, err
	}
	filter := bson.M{"_id": bson.M{"$eq": oid}}

	err = dtb.UsersColl.FindOne(context.TODO(), filter).Decode(&user)

	return user, err
}

func GetUserByName(name string) (user models.User, err error) {
	filter := bson.M{"name": bson.M{"$eq": name}}
	err = dtb.UsersColl.FindOne(context.TODO(), filter).Decode(&user)
	return user, err
}

func GetUserByEmail(email string) (user models.User, err error) {
	filter := bson.M{"email": bson.M{"$eq": email}}
	err = dtb.UsersColl.FindOne(context.TODO(), filter).Decode(&user)
	return user, err
}

func CheckUser(nameEmail string, password string) (user models.User, err error) {
	user, err = GetUserByName(nameEmail)
	if err != nil { // user found
		user, err = GetUserByEmail(nameEmail)
		if err != nil {
			return user, errors.New("user not found")
		}
	}
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err != nil {
		logs.Error.Println("[CheckUser]: ", err)
		return user, errors.New("passwords doesn't match")
	}
	user.Password = ""
	user.ID = user.ObjectID.Hex()
	return user, nil
}

func UpdatePassword(nameEmail string, oldPassword string, newPassword string) (ok bool) {

	user, err := CheckUser(nameEmail, oldPassword)
	if err != nil {
		return false
	}

	oid, err := primitive.ObjectIDFromHex(user.ID)
	if err != nil {
		return false
	}
	filter := bson.M{"_id": bson.M{"$eq": oid}}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(newPassword), bcrypt.DefaultCost)
	if err != nil {
		return false
	}

	update := bson.M{
		"$set": bson.D{{Key: "password", Value: string(hashedPassword)}},
	}

	_, err = dtb.UsersColl.UpdateOne(
		context.Background(),
		filter,
		update,
	)

	if err != nil {
		return false
	}

	return true
}
