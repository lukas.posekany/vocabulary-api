package dtb

import (
	"context"
	"os"
	"strings"

	"gitlab.com/lukas.posekany/vocabulary-api/src/logs"
	"gitlab.com/lukas.posekany/vocabulary-api/src/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/crypto/bcrypt"
)

var (
	dtbClient  *mongo.Client
	WordsColl  *mongo.Collection
	GroupsColl *mongo.Collection
	UsersColl  *mongo.Collection
)

func init() {
	host := os.Getenv("DB_HOST")
	if host == "" {
		host = "localhost"
	}
	port := os.Getenv("DB_PORT")
	if port == "" {
		port = ":27017"
	}
	user := os.Getenv("DB_USER")
	pw := os.Getenv("DB_PW")
	auth := ""
	if user != "" && pw != "" {
		auth = user + ":" + pw + "@"
	}

	mongoUrl := "mongodb://" + auth + host + port + "/vocabulary/?connect=direct"
	clientOpts := options.Client().ApplyURI(mongoUrl)
	var err error
	dtbClient, err = mongo.Connect(context.TODO(), clientOpts)
	if err != nil {
		logs.Error.Fatalln("[init dtb 1]", err)
	}

	// Check the connection
	err = dtbClient.Ping(context.TODO(), nil)

	if err != nil {
		logs.Error.Fatalln("[init dtb 2]", err)
	}

	WordsColl = dtbClient.Database("vocabulary").Collection("words")
	GroupsColl = dtbClient.Database("vocabulary").Collection("groups")
	UsersColl = dtbClient.Database("vocabulary").Collection("users")

	initAdmin()
	logs.Info.Println("DTB DONE")

}

// call this only at init of program, to make sure admin is in database
func initAdmin() {

	var adminUser bson.M
	if err := UsersColl.FindOne(context.TODO(), bson.M{"name": "superAdmin"}).Decode(&adminUser); err != nil {
		logs.Error.Println("[initAdmin 1] NOT ADMIN FOUND, creating new one")
		// ADMIN NOT FOUND
		var U models.User
		U.Name = "superAdmin"
		U.Role = "admin"
		U.Password = string("admin 007")

		_, err := AddSuperAdmin(U)
		if err != nil {
			logs.Error.Println("[InitAdmin2]: cannot init admin")
		} else {
			logs.Info.Println("[InitAdmin3]: ADDED ADMIN: new super admin was created")
		}
	} else {
		logs.Info.Println("[initAdmin4]: superAdmin was already in database")
	}
}

// addWords will take words set, add it to database and return ID of new document with error
func AddSuperAdmin(U models.User) (ID string, err error) {
	U.ObjectID = primitive.NewObjectID()

	// Hashing the password with the default cost of 10
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(U.Password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	U.Password = string(hashedPassword)

	result, err := UsersColl.InsertOne(context.TODO(), U)
	if err != nil {
		logs.Error.Println("[AddSuperAdmin1]: addUser: ", err)
	}

	if oid, ok := result.InsertedID.(primitive.ObjectID); ok {
		id := strings.Split(oid.String(), "\"")[1]
		return id, nil
	} else {
		logs.Error.Println("[AddSuperAdmin2]SOMETHING WRONG HAPPEND")
		return "", nil
	}
}
