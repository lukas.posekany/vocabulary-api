package models

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Group struct {
	ObjectID   primitive.ObjectID `json:"-" bson:"_id"`
	ID         string             `json:"id" bson:"-"`
	Name       string             `json:"name"`
	Icon       string             `json:"icon"`
	Vocabulary []string           `json:"vocabulary"`
}

func (G Group) GroupToBson() (bsonGroup primitive.D) {
	bsonGroup = bson.D{
		{Key: "name", Value: G.Name},
		{Key: "icon", Value: G.Icon},
		{Key: "vocabulary", Value: G.Vocabulary},
	}
	return bsonGroup
}
