package models

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {
	ObjectID       primitive.ObjectID `json:"-" bson:"_id"`
	ID             string             `json:"id" bson:"-"`
	Name           string             `json:"name"`
	Role           string             `json:"role"`
	Email          string             `json:"email"`
	Password       string             `json:"password"`
	Words          []string           `json:"words"`
	Groups         []string           `json:"groups"`
	Favorite       []string           `json:"favorite"`
	FavoriteGroups []string           `json:"favoriteGroups"`
}

func (U User) UserToBson() (bsonUser primitive.D) {
	bsonUser = bson.D{
		{Key: "name", Value: U.Name},
		{Key: "role", Value: U.Role},
		{Key: "email", Value: U.Email},
		{Key: "words", Value: U.Words},
		{Key: "groups", Value: U.Groups},
		{Key: "favorite", Value: U.Favorite},
		{Key: "favoriteGroups", Value: U.FavoriteGroups},
	}
	return bsonUser
}
