package models

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Words struct {
	ObjectID     primitive.ObjectID `json:"-" bson:"_id"`
	ID           string             `json:"id" bson:"-"`
	Name         string             `json:"name"`
	Group        string             `json:"group"`
	Icon         string             `json:"icon"`
	AllowedGames []string           `json:"allowedGames"`
	Languages    []string           `json:"languages"`
	Words        [][]string         `json:"words"`
}

func (W Words) WordsToBson() (bsonWords primitive.D) {

	bsonWords = bson.D{
		{Key: "name", Value: W.Name},
		{Key: "group", Value: W.Group},
		{Key: "icon", Value: W.Icon},
		{Key: "allowedGames", Value: W.AllowedGames},
		{Key: "languages", Value: W.Languages},
		{Key: "words", Value: W.Words},
	}

	return bsonWords

}
