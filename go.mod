module gitlab.com/lukas.posekany/vocabulary-api

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/sessions v1.2.1
	go.mongodb.org/mongo-driver v1.5.0
	golang.org/x/crypto v0.0.0-20200302210943-78000ba7a073
)
