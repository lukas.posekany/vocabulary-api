package main

import (
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"gitlab.com/lukas.posekany/vocabulary-api/src/logs"
	"gitlab.com/lukas.posekany/vocabulary-api/src/middlewares"
	"gitlab.com/lukas.posekany/vocabulary-api/src/routes"
)

func main() {
	r := mux.NewRouter()

	// http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("./src/public"))))
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./src/public"))))

	r.HandleFunc("/words/getWords", routes.GetWords)
	r.HandleFunc("/words/editWords", routes.ProcessWords)
	r.HandleFunc("/words/deleteWords", routes.DeleteWords)

	r.HandleFunc("/groups/getGroups", routes.GetGroups)
	r.HandleFunc("/groups/editGroups", routes.ProcessGroup)
	r.HandleFunc("/groups/deleteGroups", routes.DeleteGroup)
	r.HandleFunc("/groups/addToGroup", routes.AddToGroup)

	r.HandleFunc("/users/updateUserArray", routes.UpdateUserArray)
	r.HandleFunc("/users/updatePwd", routes.UpdatePwd)
	r.HandleFunc("/users/getCurrentUser", routes.GetCurrentUser)

	r.HandleFunc("/authentication/login", routes.Login)
	r.HandleFunc("/authentication/logout", routes.Logout)

	r.HandleFunc("/admin/listUsers", routes.GetAllUsers)
	r.HandleFunc("/admin/editUser", routes.ProcessUser)
	r.HandleFunc("/admin/deleteUser", routes.DeleteUser)

	http.Handle("/words/", middlewares.AuthenticatedMiddleware(r))
	http.Handle("/groups/", middlewares.AuthenticatedMiddleware(r))
	http.Handle("/users/", middlewares.AuthenticatedMiddleware(r))

	http.Handle("/authentication/", middlewares.UserCorsMiddleware(r))
	http.Handle("/admin/", middlewares.SuperAuthenticatedMiddleware(r))

	port := os.Getenv("PORT")
	if port == "" {
		port = ":8080"
	}

	logs.Error.Fatal(http.ListenAndServe(port, nil))
}
